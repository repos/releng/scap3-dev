# Scap3 development environment

An environment consisting of Docker containers for developing and testing Scap3
code, Scap's self-installer (scap-over-scap), and particular Scap3 deployments.

## Current components

 * Deployment servers (Primary & Secondary)
 * LDAP server
 * Jenkins targets (CI & Releasing)
 * Phorge target & DB

## Requirements

 * docker: Tested with versions 20.10.12, 23.0.1
 * docker-compose: version 2 (must be the Docker plugin)
 * OS: Should work with debian 10/11, Ubuntu 20.04+, Mint 20.3+. Not tested on
   Mac.

## Interacting with the environment

The environment is managed using the `./scap3_dev.sh` script. Run
`./scap3_dev.sh -h` to see available options.

A workspace is created during env creation, it will contain copies of Scap and
the Scap3 services being deployed. You can modify the code in the workspace to
test your changes.

## Privileged containers

In order to run `systemd` as an entry point under Docker, the Jenkins
containers run privileged. To disable use `./scap3_dev.sh --unprivileged ...`.

To remember this option, you can create a `scap3_dev.env` file which, when it
exists, will be sourced by the script:

```
PRIVILEGED=false
```

## Adding a new service

Add to:
 * `docker-compose.yaml`
 * List of CURRENT_SERVICES in `scap3_dev.sh`
 * List of known hosts in `Dockerfile.base`
 * Appropriate target file or files, i.e.: `scap-targets-all`, `scap-masters`, etc

## Deploying the services

Connect to the `deploy` host with `./scap3_dev.sh con deploy`. On the root dir,
`./deploy_services.sh` can be used to deploy all defined services (default) or
deploy a particular one via a command line argument.

Running this once will clone deployment repos to `/srv/deployment`.

Currently available services: `jenkins`, `phorge`.

`./deploy_services.sh` can also be used to update scap.

Run `./deploy_services.sh -h` to see available options.

## Accessing the services

### Jenkins

Web interface of the CI & Releasing Jenkins instances is currently exposed on
the local host on ports 8081 & 8082 respectively.

LDAP user with credentials `relenger/password` has full access to both
instances.

In order to avoid noise/potential issues, all pre-defined jobs are disabled on
startup. These can be re-enabled manually if required.

### Phorge

Phorge blocks access for any requests not using a hostname with a domain name.
You will need to add the two following names to your `/etc/hosts` file:
`phorge.lan`, `phorge.wmfusercontent.lan`. Then you can reach the web UI at
`http://phorge.lan:8083/`.

An admin with credentials `phabenger/Password123` is already configured. The
user is local, due to [^this change] it seems we would need a
code change to allow the LDAP auth provider to work outside of the production
environment.

[^this change]: https://gitlab.wikimedia.org/repos/phabricator/phabricator/-/commit/12b9d17b042f98d393737f4f8a0dea16e70d8732

xdebug is installed on the instance and configured with port 9003. You can use
an SSH tunnel to connect a debugging tool:

```
ssh -i <scap_deploy_key> -N -R 9003:localhost:9003 scap-deploy@localhost -p 2322
```

You can find all local customizations in script `phab_deploy_finalize`.
Temporary changes in that file may be necessary when testing modifications that
reflect only the production setup, such as e.g. verifying that a certain config
change would apply in prod.

## QEMU virtual machine

On some systems, we have observed the systemd running inside privileged
containers leak and replace the host's own systemd process. When this happens,
the hostname will be replaced and/or the X Window session may crash.

If you run into this issue, you can use the `qemu_vm.sh` script in `qemu-vm` to
create a VM where you can safely run the environment. Run `qemu_vm.sh -h` to
see available options.

Once the VM is up and finished bootstrapping, you can connect with `qemu_vm.sh
con`. You will find the `scap3-dev` repo already checked out in the user's
home.

Services running in the environment (e.g. Jenkins) will be exposed on the host
via the VM's IP. You can retrieve the IP with `qemu_vm.sh show-ip`.

### Requirements

#### Hardware

 * Any modern CPU should do
 * At least 2G of available memory. By default the VM uses 4G
 * 15G of free disk

#### Debian Packages

 * libvirt-daemon-system
 * virtinst

After installing the packages, make sure the user you will use to run the VM is
in group `libvirt`. You will get errors otherwise.

Also, on Debian hosts, the `default` network created won't be started
automatically. You will need to run:

```
virsh --connect=qemu:///system net-start default
```

To make it start automatically use:

```
virsh --connect=qemu:///system net-autostart default
```
