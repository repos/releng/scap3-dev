#!/usr/bin/env bash

set -eu -o pipefail

function parse_args {
  UPDATE_SCAP=n
  SERVICE=

  local PROCESSING_OPS=y

  while [[ "$PROCESSING_OPS" == y ]] && (($# > 0)); do
    case "$1" in
      --update-scap)
        UPDATE_SCAP=y
        shift
        ;;
      -h|--help|-*)
        usage && exit 0
        ;;
      *)
        PROCESSING_OPS=n
        ;;
    esac
  done

  if (($# > 0)); then
    SERVICE=$1
  fi
}

function usage {
  echo "
  Usage: $(basename "$0") [SERVICE]

  Deploys all known Scap3 services. Alternatively, a single specific SERVICE can be deployed

  Options:
    --update-scap  Update scap before deploying services
"
}

function info {
  echo
  echo "== $1 =="
}

function update_scap {
  info "Updating scap"

  sudo -su scap /update_scap.sh "$SERVICES_DIR"/scap
}

# Copy the original service code to the deployment location
function copy_service {
  local SERVICE="$SERVICES_DIR"/$1
  local DEPLOYMENT="$DEPLOYMENT_DIR"/$2

  mkdir -p "$DEPLOYMENT"
  rsync -prl "$SERVICE" "$DEPLOYMENT"
  # Use env config for service
  rsync -pr /services_conf/"$1" "$DEPLOYMENT"
  # Make sure the remote's URL is configured to use HTTP. Avoids potential problems with LFS files
  REMOTE_URL=$(git -C "$DEPLOYMENT/$1" ls-remote --get-url)
  HTTP_URL="${REMOTE_URL//git@gitlab.wikimedia.org:/https:\/\/gitlab.wikimedia.org/}"
  git -C "$DEPLOYMENT/$1" remote set-url origin "$HTTP_URL"
}

function deploy_service {
  local SERVICE="$DEPLOYMENT_DIR"/$1
  local DEPLOY_SCRIPT=
  local DEPLOY_SCRIPT_ARGS=

  if (($# > 1)); then
    DEPLOY_SCRIPT=$2
  fi
  if (($# > 2)); then
    DEPLOY_SCRIPT_ARGS=$3
  fi

  if [[ -z "$DEPLOY_SCRIPT" ]]; then
    (cd "$SERVICE" && scap deploy)
  else
    (cd "$SERVICE" && ./"$DEPLOY_SCRIPT" "$DEPLOY_SCRIPT_ARGS")
  fi
}

# The copy destination locations in the functions below must match the defined `git_repo` Scap config value in the
# corresponding deploy repositories

function deploy_jenkins {
  info "Deploying Jenkins"

  copy_service jenkins-deploy releng
  deploy_service releng/jenkins-deploy deploy.sh "-y"
}

function deploy_phorge {
  info "Deploying Phorge"

  # If the destination dir is changed here, there is a symlink in the Phorge service Dockerfile that needs to be
  # updated too
  copy_service phorge-deploy/ phabricator/deployment
  deploy_service phabricator/deployment
}

function deploy_all {
  deploy_jenkins
  deploy_phorge
}

parse_args "$@"

DEPLOYMENT_DIR=/srv/deployment
SERVICES_DIR=/srv/services

if [[ "$UPDATE_SCAP" == y ]]; then
  update_scap
fi
if [[ -z "$SERVICE" ]]; then
  deploy_all
else
  deploy_"$SERVICE"
fi
