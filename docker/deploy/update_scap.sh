#!/usr/bin/env bash

# Installs scap locally and on all targets using a locally-generated Python venv. Wheels are skipped to make things
# faster
#
# Must be run as user "scap"

set -eu -o pipefail

if [ "$(id -un)" != scap ]; then
  echo 'Run me as user "scap"'
  exit 1
fi

VENV=$HOME/scap
SCAP_SOURCE=$1

SOURCE_COPY=/tmp/scap_copy
rm -rf "$SOURCE_COPY"
# Copy to work around not having write permissions
cp -r "$SCAP_SOURCE/" $SOURCE_COPY
# This directory is now necessary to build and install a package from $SCAP_SOURCE. This happened after SpiderPig was
# added to Scap. As of this writing, the dir gets created as part of Scap's image build pipeline:
# https://gitlab.wikimedia.org/repos/releng/scap/-/blob/252275951cd52e94040d21145d299984ddf18678/.pipeline/blubber.yaml#L128
# The dev environment doesn't currently support SpiderPig and this is just a quick workaround. Chances are this breaks
# again in the future
mkdir -p "$SOURCE_COPY"/web/dist/assets

# On very rare occasions, this command fails with:
# "The virtual environment was not created successfully because ensurepip is not available. On Debian/Ubuntu systems,
# you need to install the python3-venv package [...]"
# It's not clear what causes the problem, since python3-venv is already installed on all images by the time containers
# start. To work around the problem, we retry the command once in case of error
if ! python3 -m venv --clear "$VENV"; then
  python3 -m venv --clear "$VENV"
fi
"$VENV"/bin/pip3 install wheel
"$VENV"/bin/pip3 install "$SOURCE_COPY"

# shellcheck disable=SC1004
while read -r TARGET; do
  ssh -n "$TARGET" '
    rsync \
    --archive --delay-updates --delete --delete-delay \
    --compress --new-compress --exclude=*.swp --exclude=**/__pycache__ \
    deploy::scap-install-staging/scap/ $HOME/scap/
  '
done < /etc/dsh/group/scap_targets
