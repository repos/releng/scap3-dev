#!/bin/bash
set -eu -o pipefail

# Used https://gitlab.wikimedia.org/repos/releng/dev-images/-/blob/main/dockerfiles/bullseye-openldap/entrypoint.sh as
# a basis. I don't claim to understand all the details of the LDAP configuration

LDAP_ROOT='dc=wikimedia,dc=org'
LDAP_ADMIN_USERNAME="cn=admin,$LDAP_ROOT"
LDAP_ADMIN_PASSWORD=adminpassword
LDAP_ENCRYPTED_ADMIN_PASSWORD="$(echo -n "$LDAP_ADMIN_PASSWORD" | slappasswd -n -T /dev/stdin)"

cat << EOF > /etc/ldap/slapd.conf
# Schema and objectClass definitions
include         /etc/ldap/schema/core.schema
include         /etc/ldap/schema/cosine.schema
include         /etc/ldap/schema/rfc2307bis.schema
include         /etc/ldap/schema/inetorgperson.schema

pidfile /var/run/slapd/ldap.pid

modulepath  /usr/lib/ldap
moduleload  back_mdb
moduleload  back_monitor
moduleload  memberof
moduleload  syncprov
moduleload  auditlog
moduleload  ppolicy
moduleload  deref
moduleload  unique

database            mdb
suffix              $LDAP_ROOT
directory           /var/lib/ldap/
rootdn              "$LDAP_ADMIN_USERNAME"
readonly            false
rootpw              $LDAP_ENCRYPTED_ADMIN_PASSWORD

overlay unique
unique_uri ldap:///?uidNumber?sub?(objectClass=posixaccount)
unique_uri ldap:///?gidNumber?sub?(objectClass=posixgroup)
unique_uri ldap:///?cn?sub?(objectClass=posixaccount)

TLSCACertificateFile /etc/ldap/certs/scap3-dev-ca.crt
TLSCertificateFile /etc/ldap/certs/scap3-dev-ldap.crt
TLSCertificateKeyFile /etc/ldap/certs/scap3-dev-ldap.key

# The userPassword by default can be changed
# by the entry owning it if they are authenticated.
# Others should not be able to see it, except the
# admin entry below
# These access lines apply to database #1 only
access to attrs=userPassword,shadowLastChange
        by dn="$LDAP_ADMIN_USERNAME" write
        by anonymous auth
        by self write
        by * none

# Ensure read access to the base for things like
# supportedSASLMechanisms.  Without this you may
# have problems with SASL not knowing what
# mechanisms are available and the like.
# Note that this is covered by the 'access to *'
# ACL below too but if you change that as people
# are wont to do you'll still need this if you
# want SASL (and possible other things) to work
# happily.
access to dn.base="" by * read

# everyone can read everything else not already defined
# in above rules and write self
access to *
        by self write
        by * read

EOF

is_alive() {
  fail=0
  while [ $fail -le 30 ]; do
    if ! r="$(ldapsearch -x -H ldap://localhost:389 -D "$LDAP_ADMIN_USERNAME" -w "$LDAP_ADMIN_PASSWORD" 2>/dev/null | grep -c result)"
    then
      [ "$r" -gt 0 ] && return 0
    fi
    fail=$((fail + 1))
    sleep 1
  done
  echo "waiting for is alive failed"
  exit
}

ldap_load_data() {
  is_alive
  for ldif_file in /ldifs/*.ldif; do
    ldapadd -f "$ldif_file" -x -D "$LDAP_ADMIN_USERNAME" -w "$LDAP_ADMIN_PASSWORD"
  done
  return 0
}

ldap_load_data &
# By default LDAP will listen on ports ldap:389 and ldaps:636
slapd -f /etc/ldap/slapd.conf -d 256 -h "ldap:/// ldaps:///"
