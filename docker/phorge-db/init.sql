CREATE USER IF NOT EXISTS 'phuser' IDENTIFIED BY 'password';
CREATE USER IF NOT EXISTS 'phabricatorphd' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'phuser';
GRANT ALL PRIVILEGES ON *.* TO 'phabricatorphd';
