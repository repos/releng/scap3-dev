import jenkins.model.Jenkins

// Disable all jobs on startup

Jenkins.instance.getAllItems().each {
    it.makeDisabled(true)
}
