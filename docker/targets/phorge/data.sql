
-- Configure a local basic auth provider and an admin user phabenger/Password123

INSERT INTO phabricator_auth.auth_providerconfig VALUES (
  1,'PHID-AUTH-vnaxna3tghnqlerlr2rl','PhabricatorPasswordAuthProvider','password','self',1,1,1,1,1,0,'[]',1695219444,1695219444,0
);

INSERT INTO phabricator_user.user VALUES (
  1,'PHID-USER-2rcw3rdnumnqy5ciw7f2','phabenger','phabricator relenger',1695219568,1695219677,NULL,
  'g76646fpy4l75htdhob76bkoae2opn4lvz43bfs5zqs6wzw357xxkruvo56znrwomhzwb2lssvcrmy3diknugztfguutz2ja55pgzvjzju3fawytija3dym65eln7zqgzc2t5syryk7cy5rrnkonrujmft6fpkc26ukq5fibewusncszyu76dgrdpomdhg4yhighng2pdm7jobpoolzub26ryibqgmcaw3agw5jhb6tow4v62q4moxgem4gqcwp',
  0,0,1,1,1,'yxjrqcxdtn52lr75behe3oupuvuvbspgai7gzcjktayy7lztzoucaayr3lssrsqa',0,'{\"until\":null,\"eventPHID\":null,\"availability\":null}',1695478835,0,NULL,NULL
);

INSERT INTO phabricator_auth.auth_password VALUES (
  1,'PHID-APAS-iywqjbhtt74dlvc42bdy','PHID-USER-2rcw3rdnumnqy5ciw7f2','account','bcrypt:$2y$11$j.GZe0bXd1440Ii2rRD0SuuY6QvINU2nDY4CmKG3O6oZL2AqDT66e'
  ,0,1695219568,1695219568,'cvlwovgmsevetd5y5x5ox27mzj2kbb4dkotjrl6ye5wsjt36mfblbjalunishemv',NULL
);

INSERT INTO phabricator_user.user_externalaccount VALUES (
  1,'PHID-XUSR-6o7zk2lqqpnnxptyjz5c','PHID-USER-2rcw3rdnumnqy5ciw7f2','password','self','tfebdv7m32hlxdvqamivhtcrj7j2ihpm',
  '',NULL,1695219568,1695219568,NULL,NULL,NULL,0,NULL,NULL,'[]','PHID-AUTH-vnaxna3tghnqlerlr2rl'
);

INSERT INTO phabricator_user.user_email VALUES (
  1,'PHID-USER-2rcw3rdnumnqy5ciw7f2','phabenger@fake-wikimedia.org',1,1,'fjnthhyr6lb6k4bvixmgaqyx',1695219568,1695219673,'PHID-EADR-babfxgecooll4xo4lhbd'
);
