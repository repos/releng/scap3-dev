#!/usr/bin/env bash

set -eu -o pipefail

function usage {
  SCRIPT=$(basename "$0")

  echo "
  Usage: $SCRIPT [OPTIONS] SUBCOMMAND [ARGS]

  Create a QEMU debian VM to run the Scap3 development environment in

  Subcommands:
    $SCRIPT build [--low-mem] WORKSPACE SSH_KEY   Create a new VM. Disk files will be stored in WORKSPACE, this dir must exist and
                                                     be accessible by the user running this script and by user libvirt-qemu. SSH_KEY
                                                     public key file will be used to access the VM
    $SCRIPT start                                 Start the VM
    $SCRIPT stop                                  Stop the VM
    $SCRIPT restart                               Restart the VM
    $SCRIPT clean                                 Destroy the VM
    $SCRIPT con                                   Open a terminal to the VM
    $SCRIPT cp HOST_PATH VM_PATH                  Copy HOST_PATH to the vm at VM_PATH
    $SCRIPT show-ip                               Show the local IP of the VM

  Optional arguments:
    --low-mem                                        When used with 'build', assigns only 2G of memory to the VM. Default is 4G
"
}

function info {
  echo
  echo "== $1 =="
}

function from_script_dir {
  pushd "$SCRIPT_DIR" >/dev/null
  trap 'popd >/dev/null' EXIT
}

function vm_exists {
  virsh list --all --name | grep -wq "$VM_NAME"
}

function destroy_vm {
  virsh list --name | grep -wq "$VM_NAME" && virsh shutdown "$VM_NAME"
  virsh undefine "$VM_NAME"

  TO=$(( $(date +%s) + 20 ))
  while vm_exists; do
    (( $(date +%s) > "$TO" )) && echo "Failed to destroy VM. Aborting" && exit 1
    sleep 5
  done
}

function get_vm_ip {
    if ! VM_IP=$(virsh net-dhcp-leases default | grep "$VM_NAME" | awk '{print $5}' | cut -d/ -f1); then
      echo 'VM not ready for connections. Is it running?' && exit 1
    fi
}

function pre_build_checks {
  if ! ssh-keygen -l -f "$SSH_KEY" &>/dev/null; then
    echo "Provided SSH key is not valid. Aborting" && exit 1
  fi

  if ! id -nG | grep -qw libvirt; then
    echo "User needs to be a member of group 'libvirt'. Aborting" && exit 1
  fi

  if vm_exists; then
    read -r -p "VM $VM_NAME already exists. Recreate? (Y/n) " ANS
    ANS=${ANS:-y}
    if [ "$ANS" != y ]; then
      echo "Nothing to do. Finishing" && exit 0
    else
      info 'Removing VM'

      destroy_vm
    fi
  fi
}

function prepare_vars {
  {
    echo WORKSPACE="$WORKSPACE"
    echo SSH_KEY="'$SSH_KEY'"
  } > .vars
}

function prepare_workspace {
  info 'Preparing workspace'

  BASE_IMAGE=debian-11-genericcloud-amd64.qcow2

  (
    cd "$WORKSPACE"

    cp "$SCRIPT_DIR"/cloud-init/* .
    sed -i "s|<SSH_KEY>|$(tr -d '\n' <"$SSH_KEY")|g" user-data

    rm -f "$IMAGE_DISK"
    [ ! -e $BASE_IMAGE ] && curl -fL -o $BASE_IMAGE https://cdimage.debian.org/cdimage/cloud/bullseye/latest/$BASE_IMAGE
    qemu-img create -b $BASE_IMAGE -f qcow2 -F qcow2 "$IMAGE_DISK" 15G
  )
}

function create_vm {
  info 'Creating VM'

  if [ "$LOW_MEM" = y ]; then
    MEM=2048
  else
    MEM=4096
  fi

  # Passing os-variant autodetect to the command causes it to emit a warning. However passing an actual distro from
  # `virt-install --osinfo list` (e.g. debian11) causes the cloud init config to be ignored for some obscure reason
  virt-install \
    --name "$VM_NAME" \
    --memory $MEM \
    --cpu host \
    --os-variant detect=on \
    --import \
    --disk "$WORKSPACE/$IMAGE_DISK" \
    --cloud-init meta-data="$WORKSPACE"/meta-data,user-data="$WORKSPACE"/user-data,disable=on \
    --network bridge=virbr0,model=virtio \
    --graphics none \
    --noautoconsole
}

export LIBVIRT_DEFAULT_URI='qemu:///system'

VM_NAME=scap3-dev
IMAGE_DISK=scap3-dev.img
SCRIPT_DIR=$(dirname "$(realpath -m $0)")
LOW_MEM=n

(($# == 0)) && usage && exit 1

case "$1" in
  build)
    shift

    if [ "$1" = '--low-mem' ]; then
      LOW_MEM=y
      shift
    fi

    (($# != 2)) && usage && exit 1
    WORKSPACE=$(realpath -e "$1")
    SSH_KEY=$(realpath -e "$2")

    pre_build_checks
    from_script_dir
    prepare_vars
    prepare_workspace
    create_vm

    info "Scap3 dev VM created & started. Full bootstrap should not take longer than ~1m"
    ;;
  start)
    virsh start $VM_NAME

    info "Scap3 dev VM started"
    ;;
  stop)
    virsh shutdown $VM_NAME

    info "Scap3 dev VM stopped"
    ;;
  restart)
    virsh reboot $VM_NAME

    info "Scap3 dev VM restarted"
    ;;
  clean)
    from_script_dir
    if [ -e .vars ]; then
      source .vars

      read -r -p "Also remove the contents of $WORKSPACE? (Y/n) " ANS
      ANS=${ANS:-y}
      if [ "$ANS" = y ]; then
        rm -rf "${WORKSPACE:?}"/*
      else
        echo "Leaving $WORKSPACE alone"
      fi
    fi

    vm_exists && destroy_vm

    info "Scap3 dev VM deleted"
    ;;
  con)
    shift

    from_script_dir
    [ ! -e .vars ] && echo "No workspace created, SSH key to connect unknown. Aborting" && exit 1
    source .vars

    get_vm_ip
    ssh -o StrictHostKeyChecking=no -i "${SSH_KEY%.pub}" -l relenger "$VM_IP" "$@"
    ;;
  cp)
    shift
    (($# != 2)) && usage && exit 1
    HOST_PATH="$1"
    VM_PATH="$2"

    from_script_dir
    [ ! -e .vars ] && echo "No workspace created, SSH key to connect unknown. Aborting" && exit 1
    source .vars

    get_vm_ip
    rsync --del -rlpte "ssh -o StrictHostKeyChecking=no -i ${SSH_KEY%.pub}" "$HOST_PATH" relenger@"$VM_IP":"$VM_PATH"
    ;;
  show-ip)
    get_vm_ip
    echo "$VM_IP"
    ;;
  *)
    usage && exit 1
    ;;
esac
