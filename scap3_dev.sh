#!/usr/bin/env bash

set -eu -o pipefail

SCRIPT_ENV_FILE=$(dirname "$0")/scap3_dev.env

function usage {
  SCRIPT=$(basename "$0")

  echo "
  Usage: $SCRIPT [OPTIONS] SUBCOMMAND [ARGS]

  Manage the Scap3 development environment

  Options:
    --unprivileged                  Do not use privileged containers
    --scap-services=<scap_services> Use only the services in <scap_services> ('jenkins', 'phorge', etc). Only available
                                    for 'build' command

  Subcommands:
    $SCRIPT build WORKSPACE   Build and start the environment. Necessary files/dirs will be placed under WORKSPACE
    $SCRIPT start             Start the environment
    $SCRIPT stop              Stop the environment
    $SCRIPT clean             Destroy the environment
    $SCRIPT con SERVICE       Open a terminal on one of the services defined in the docker-compose file

  $SCRIPT_ENV_FILE would be sourced if it exists to set PRIVILEGED. Command
  line arguments take precedence over it.

"
}

function info {
  echo
  echo "== $1 =="
}

function docker_compose {
  (cd docker && docker compose "$@")
}

function from_script_dir {
  pushd "$(dirname $0)" >/dev/null
  trap 'popd >/dev/null' EXIT
}

function clone_repo {
  local REPO_URL=$1
  local TARGET_DIR=$2

  if [ ! -d "$TARGET_DIR" ]; then
    git clone "$REPO_URL" "$TARGET_DIR"
  fi

  if (($# > 2)) && [[ "$3" == --submodules ]]; then
    (cd "$TARGET_DIR" && git submodule init && git submodule update)
  fi
}

function clone_repos {
  info "Cloning repositories"

  clone_repo https://gitlab.wikimedia.org/repos/releng/scap.git "$SERVICES_DIR/scap"
  clone_repo https://gitlab.wikimedia.org/repos/releng/jenkins-deploy.git "$SERVICES_DIR/jenkins-deploy"
  clone_repo https://gitlab.wikimedia.org/repos/phabricator/deployment.git "$SERVICES_DIR/phorge-deploy" --submodules
}

function prepare_docker_compose_env {
  info "Preparing docker-compose .env"

{
  echo PRIVILEGED="$PRIVILEGED"
  echo SERVICES_DIR="$SERVICES_DIR"
  echo SERVICES_DIR_TARGET="$SERVICES_DIR_TARGET"
  # Use the uid/gid of the user running the dev env for the deploy user. That should give the deploy user access to
  # mounted dirs in most cases
  echo DEPLOY_USER_UID="$(id -u)"
  echo DEPLOY_USER_GID="$(id -g)"
  echo SCAP_TARGETS_FILE="$SCAP_TARGETS_FILE"
} > docker/.env
}

function create_certs {
  if [ ! -d "$CERTS_DIR" ]; then
    info "Creating certificates"

    # For a dev/test env, it might as well be indefinite
    CERTS_EXPIRATION=36500
    CA_CERT=scap3-dev-ca.crt
    CA_CERT_KEY=scap3-dev-ca.key
    LDAP_CRS=scap3-dev-ldap.csr
    LDAP_CERT=scap3-dev-ldap.crt
    LDAP_CERT_KEY=scap3-dev-ldap.key

    mkdir $CERTS_DIR
    (
      cd $CERTS_DIR

      # CA
      openssl req -nodes -x509 -newkey rsa:2048 -days $CERTS_EXPIRATION -subj "/CN=scap3-dev" \
        -out $CA_CERT -keyout $CA_CERT_KEY
      # Certificate request
      openssl req -nodes -newkey rsa:2048 -subj "/CN=ldap" \
        -out $LDAP_CRS -keyout $LDAP_CERT_KEY
      # Self-sign certificate request and get LDAP certificate
      SUBJECT_ALT_NAMES=DNS:ldap-ro.eqiad.wikimedia.org,DNS:ldap-ro.codfw.wikimedia.org
      openssl x509 -req -days $CERTS_EXPIRATION \
        -CAcreateserial -CA $CA_CERT -CAkey $CA_CERT_KEY \
        -extensions exts -extfile <(echo -e "[exts]\nsubjectAltName=$SUBJECT_ALT_NAMES") \
        -in $LDAP_CRS -out $LDAP_CERT

      chmod 600 $LDAP_CERT_KEY
    )

    # Copy certs to required locations
    cp -pr $CERTS_DIR docker/common
    cp -pr $CERTS_DIR docker/ldap
  fi
}

function start_services {
  SERVICES=$1

  info "Starting services containers"

  # At some point, the `depends_on` clause in `docker-compose.yml` stopped being honored when BuildKit creates its
  # building graph for the images. So we build the images in the right order ourselves
  for base in base-image base-systemd-image base-jenkins-image; do
      docker_compose build $base
  done
  # shellcheck disable=SC2086
  docker_compose build $SERVICES
  # shellcheck disable=SC2086
  docker_compose up -d $SERVICES
}

# Installs a fresh scap or updates existing one on all hosts
function install_scap {
  info "Installing/updating scap"

  docker_compose exec \
      --user scap \
      -w "$SERVICES_DIR_TARGET" \
      deploy /update_scap.sh "$SERVICES_DIR_TARGET"/scap
}

function install_docker {
  SERVICE=$1

  info "Installing docker on $SERVICE"

  docker_compose exec --user root "$SERVICE" apt-get update
  docker_compose exec --user root "$SERVICE" apt-get install -y --no-install-recommends docker.io
}

function install_docker_on_required_targets {
  # Install docker on jenkins-rel to be able to run docpub (https://gitlab.wikimedia.org/repos/releng/docpub)
  for SERVICE in deploy jenkins-rel; do
    if echo "$SERVICES" | grep "$SERVICE" >/dev/null; then install_docker "$SERVICE"; fi
  done
}

(($# == 0)) && usage && exit 1

declare -A CURRENT_SERVICES=([infra]='deploy deploy-secondary ldap' [jenkins]='jenkins-ci jenkins-rel' [phorge]='phorge-db phorge')
CERTS_DIR=certs

if [ -f "$SCRIPT_ENV_FILE" ]; then
  printf "Loading settings from %s\n" "$SCRIPT_ENV_FILE"
  # shellcheck source=scap3_dev.env
  source "$SCRIPT_ENV_FILE"
fi

# If ever changing the default, also change it in docker/docker-compose.yml
PRIVILEGED=${PRIVILEGED:-true}
case "$1" in
  --unprivileged)
    PRIVILEGED=false
    shift;
    ;;
esac

case "$1" in
  build)
    shift

    SERVICES=${CURRENT_SERVICES[*]}
    SCAP_TARGETS_FILE=scap-targets-all
    case "$1" in
      --scap-services=*)
        SERVICES=${CURRENT_SERVICES[infra]}
        for SCAP_SERVICE in ${1##*=}; do
          if ! echo "${!CURRENT_SERVICES[*]}" | grep "$SCAP_SERVICE" >/dev/null; then
            echo "Service \"$SCAP_SERVICE\" not recognized" && exit 1
          fi

          SERVICES="$SERVICES ${CURRENT_SERVICES[$SCAP_SERVICE]}"
          SCAP_TARGETS_FILE=scap-targets-$SCAP_SERVICE
        done
        shift
        ;;
      -*)
        usage && exit 1
        ;;
    esac

    (($# == 0)) && usage && exit 1
    WORKSPACE=$(realpath -m "$1")
    SERVICES_DIR="$WORKSPACE/services"
    SERVICES_DIR_TARGET=/srv/services

    from_script_dir
    clone_repos
    prepare_docker_compose_env
    create_certs
    start_services "$SERVICES"
    install_scap
    # On Debian 11, installing docker in the Dockerfile or entrypoint doesn't work. Possibly due to systemd not running
    # at that point. Whatever the reason, doing it here works
    install_docker_on_required_targets "$SERVICES"

    info "Environment built & started"
    ;;
  start)
    from_script_dir
    docker_compose start

    info "Environment started"
    ;;
  stop)
    from_script_dir
    docker_compose stop

    info "Environment stopped"
    ;;
  clean)
    from_script_dir
    docker_compose down --remove-orphans --volumes
    # Clean dangling images left behind by the build process
    docker images -f dangling=true -q | xargs -r docker rmi
    rm -r $CERTS_DIR

    info "Environment deleted"
    ;;
  con)
    from_script_dir
    shift
    (($# == 0)) && usage && exit 1
    SERVICE=$1

    docker_compose exec --user "$(id -u)" "$SERVICE" bash
    ;;
  *)
    usage && exit 1
    ;;
esac
